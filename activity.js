// 2. Count fruits on sale
db.fruits.count({
	"onSale": true
},
{
	"fruitsOnSale": "$count"
});

// 3. Count total number of fruits with stock more than 20
db.fruits.count({
	"stock": {
		$gte: 20
	}
},
{
	"enoughStock": "$count"
});

// avg price of fruits on sale per supplier
db.fruits.aggregate([
	{
		$match: {
			"onSale": true
		}
	},
	{
		$group: {
			"_id": "$supplier_id",
			"average_price": {
				$avg: "$price"
			}
		}
	}
]);

// max price of fruit per supplier
db.fruits.aggregate([
	{
		$match: {
			
		}
	},
	{
		$group: {
			"_id": "$supplier_id",
			"max_price": {
				$max: "$price"
			}
		}
	}
]);

// min price of fruit per supplier
db.fruits.aggregate([
	{
		$group: {
			"_id": "$supplier_id",
			"min_price": {
				$min: "$price"
			}
		}
	}
]);